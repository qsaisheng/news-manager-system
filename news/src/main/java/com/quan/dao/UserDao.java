package com.quan.dao;

import java.util.List;
import java.util.Map;

import com.quan.pojo.User;

public interface UserDao {
	User login(String username,String password);
	int register(String username,String password);
	//查询注册是否用户名重复
	int selectUsername(String username);
	//查询所有用户
	List<User> selectAll();
	User selectOne(int uid);
	//修改用户
	int updateUser(User user);
	int deleteUser(int uid);
	int selectUserCounts(Map<String, Object> map);
	List<User> selectByLimit(Map<String,Object> map);
}
