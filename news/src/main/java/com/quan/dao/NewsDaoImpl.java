package com.quan.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.quan.pojo.News;

public class NewsDaoImpl extends BaseDao implements NewsDao{
	@Override
	public int selectCounts(Map<String,Object> map) {
		int flag=0;
		List<Object> maplist = new ArrayList<>();
		StringBuffer sb = new StringBuffer();
		sb.append("select count(1) from news where 1=1"); 
		if(map.containsKey("ntid")) {
			sb.append(" and ntid=?");
			maplist.add(map.get("ntid"));
		}
		if(map.containsKey("ntitle")) {
			sb.append(" and ntitle like ?");
			maplist.add("%"+map.get("ntitle")+"%");
		}
		Object[] params=maplist.toArray();
		rs=this.executeQuery(sb.toString(), params);
		try {
			while(rs.next()) {
				flag=rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			this.closeAll();
		}
		return flag;
	}

	@Override
	public List<News> selectByLimit(Map<String,Object> map) {
		StringBuffer sb=new StringBuffer();
		List<Object> maplist = new ArrayList<>();
		sb.append("select * from news where 1=1");
		if(map.containsKey("ntid")) {
			sb.append(" and ntid=?");
			maplist.add(map.get("ntid"));
		}
		if(map.containsKey("ntitle")) {
			sb.append(" and ntitle like ?"); 
			maplist.add("%"+map.get("ntitle")+"%");
		}
		sb.append(" order by `ncreateDate` desc");
		if(map.containsKey("start")&&map.containsKey("pageSize")) {
			sb.append(" limit ?,?");
			maplist.add(map.get("start"));
			maplist.add(map.get("pageSize"));
		}
		Object[] params=maplist.toArray();
		this.rs=this.executeQuery(sb.toString(), params);
		List<News> newsList = new ArrayList<>();
		try {
			while(rs.next()) {
				News news = new News();
				news.setNid(rs.getInt(1));
				news.setNtid(rs.getInt(2));
				news.setNtitle(rs.getString(3));
				news.setNauthor(rs.getString(4));
				news.setNcreateDate(rs.getTimestamp(5));
				news.setNpicPath(rs.getString(6));
				news.setNcontent(rs.getString(7));
				news.setNmodifyDate(rs.getTimestamp(8));
				news.setNsummary(rs.getString(9));
				newsList.add(news);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			this.closeAll();
		}
		return newsList;
	}

	@Override
	public News selectByNid(int nid) {
		String sql="select * from news where nid=?";
		Object[] params = {nid};
		rs=this.executeQuery(sql, params);
		News news = new News();
		try {
			while(rs.next()) {
				news.setNid(rs.getInt(1));
				news.setNtid(rs.getInt(2));
				news.setNtitle(rs.getString(3));
				news.setNauthor(rs.getString(4));
				news.setNcreateDate(rs.getTimestamp(5));
				news.setNpicPath(rs.getString(6));
				news.setNcontent(rs.getString(7));
				news.setNmodifyDate(rs.getTimestamp(8));
				news.setNsummary(rs.getString(9));
			}	
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			this.closeAll();
		}
		return news;
	}
	@Override
	public int update(News news) {
		String sql="update news set ntid=?,ntitle=?,nauthor=?,npicPath=?,ncontent=?,nmodifyDate=?,nsummary=? where nid=?";
		Object[] params= {news.getNtid(),news.getNtitle(),news.getNauthor(),news.getNpicPath(),news.getNcontent(),news.getNmodifyDate(),news.getNsummary(),news.getNid()};
		int flag=-1;
		flag=this.executeUpdate(sql, params);
		this.closeAll();
		return flag;
	}

	@Override
	public int add(News news) {
		String sql="insert into news(ntid,ntitle,nauthor,npicPath,ncontent,nmodifyDate,ncreateDate,nsummary) values(?,?,?,?,?,?,?,?)";
		Object[] params= {news.getNtid(),news.getNtitle(),news.getNauthor(),news.getNpicPath(),news.getNcontent(),news.getNmodifyDate(),news.getNcreateDate(),news.getNsummary()};
		int flag=this.executeUpdate(sql, params);
		this.closeAll();
		return flag;
	}

	@Override
	public int selectByTid(int tid) {
		String sql="select * from news where ntid=?";
		Object[] params= {tid};
		rs=this.executeQuery(sql, params);
		int flag=0;
		try {
			while (rs.next()) {
				flag+=1;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			this.closeAll();
		}
		return flag;
	}

	@Override
	public int deleteByNid(int nid) {
		String sql="delete from news where nid=?";
		Object[] params= {nid};
		int flag=this.executeUpdate(sql, params);
		this.closeAll();
		return flag;
	}

	@Override
	public int deleteNewsAndComments(int nid) throws SQLException {
		int flag=-1;
		try {
			conn=this.getConnection();
			conn.setAutoCommit(false);
			String sql="select * from news where nid=?";
			ps=conn.prepareStatement(sql);
			ps.setObject(1, nid);
			rs=ps.executeQuery();
			News news = new News();
			while(rs.next()) {
				news.setNid(rs.getInt(1));
			}
			//ɾ������
			String sql2="delete from comments where cnid=?";
			ps=conn.prepareStatement(sql2);
			ps.setObject(1, news.getNid());
			ps.executeUpdate();
			//ɾ������
			String sql3="delete from news where nid=?";
			ps=conn.prepareStatement(sql3);
			ps.setObject(1, nid);
			flag=ps.executeUpdate();
			conn.commit();
		}catch(Exception e) {
			conn.rollback();
			e.printStackTrace();
		}finally {
			conn.setAutoCommit(true);
			this.closeAll();
		}
		return flag;
	}

}
