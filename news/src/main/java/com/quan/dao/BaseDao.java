package com.quan.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BaseDao {
	static String driver="com.mysql.cj.jdbc.Driver";
	static String url="jdbc:mysql://localhost:3306/exam?useSSL=true&useUnicode=true&characterEncoding=utf8";
	static String username="root";
	static String password="root";
	protected Connection conn;
	protected PreparedStatement ps;
	protected ResultSet rs;
	
	public Connection getConnection() {
		try {
			Class.forName(driver);
			conn=DriverManager.getConnection(url,username,password);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public void closeAll() {
		try {
			if(rs!=null) {
				rs.close();
				rs=null;
			}
			if(ps!=null) {
				ps.close();
				ps=null;
			}
			if(conn!=null) {
				conn.close();
				conn=null;
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 公共查询方法
	 */
	public ResultSet executeQuery(String sql,Object[] params) {
		conn=this.getConnection();
		try {
			ps=conn.prepareStatement(sql);
			for(int i=0;i<params.length;i++) {
				ps.setObject(i+1, params[i]);
			}
			rs=ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	/**
	 * 公共修改方法
	 */
	public int executeUpdate(String sql,Object[] params) {
		conn=this.getConnection();
		int updateRows=0;
		try {
			ps=conn.prepareStatement(sql);
			for(int i=0;i<params.length;i++) {
				ps.setObject(i+1, params[i]);
			}
			updateRows=ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return updateRows;
	}
}
