package com.quan.dao;

import java.util.List;

import com.quan.pojo.Topic;

public interface TopicDao {
	List<Topic> selectAll();
	Topic selectOne(int tid);
	int addOne(String tname);
	Topic selectOneByName(String tname);
	int deleteByTid(int tid);
	int updateTopic(int tid,String tname);
	
}
