package com.quan.dao;

import java.util.List;
import java.util.Map;

import com.quan.pojo.Comments;

public interface CommentDao {
	//查询当前新闻评论的数量
	int selectCommentsCounts(Map<String,Object> map);
	//查询当前新闻的所有评论
	List<Comments> selectAllCommentsByCnid(int cnid);
	//添加评论
	int addComments(int cnid,String cauthor,String ccontent,String cip);
	//删除评论
	int deleteComments(int cid);
	List<Comments> selectByLimit(Map<String,Object> map);
	//查询5个
	List<Comments> select5();
	//通过cnid删除评论
	int deleteCommentsByCnid(int cnid);
}
