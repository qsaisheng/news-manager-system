package com.quan.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.quan.pojo.Comments;
import com.quan.pojo.News;

public class CommentDaoImpl extends BaseDao implements CommentDao{

	@Override
	public List<Comments> selectAllCommentsByCnid(int cnid) {
		String sql="select * from comments where cnid=?";
		Object[] params= {cnid};
		rs=this.executeQuery(sql, params);
		List<Comments> commentsList = new ArrayList<Comments>();
		try {
			while(rs.next()) {
				Comments comments = new Comments();
				comments.setCid(rs.getInt(1));
				comments.setCnid(rs.getInt(2));
				comments.setCcontent(rs.getString(3));
				comments.setCdate(rs.getTimestamp(4));
				comments.setCip(rs.getString(5));
				comments.setCauthor(rs.getString(6));
				commentsList.add(comments);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			this.closeAll();
		}
		return commentsList;
	}

	@Override
	public int addComments(int cnid, String cauthor,String ccontent,String cip) {
		String sql="insert into comments(cnid,ccontent,cdate,cip,cauthor) values(?,?,?,?,?)";
		int flag=-1;
		Date cdate = new Date();
		Object[] params= {cnid,ccontent,cdate,cip,cauthor};
		flag=this.executeUpdate(sql, params);
		this.closeAll();
		return flag;
	}

	@Override
	public int deleteComments(int cid) {
		String sql="delete from comments where cid=?";
		Object[] params= {cid};
		int flag=this.executeUpdate(sql, params);
		this.closeAll();
		return flag;
	}

	@Override
	public int selectCommentsCounts(Map<String,Object> map) {
		StringBuffer sb=new StringBuffer();
		sb.append("select count(1) from comments where 1=1");
		List<Object> maplist = new ArrayList<>();
		if(map.containsKey("cnid")) {
			sb.append(" and cnid = ?");
			maplist.add(map.get("cnid"));
		}
		Object[] params=maplist.toArray();
		this.executeQuery(sb.toString(), params);
		int flag=0;
		try {
			while(rs.next()) {
				flag=rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			this.closeAll();
		}
		return flag;
	}

	@Override
	public List<Comments> selectByLimit(Map<String,Object> map) {
		StringBuffer sb= new StringBuffer();
		sb.append("select * from comments where 1=1");
		List<Object> maplist = new ArrayList<>();
		if(map.containsKey("cnid")) {
			sb.append(" and cnid = ?");
			maplist.add(map.get("cnid"));
		}
		sb.append(" order by `cdate` desc");
		if(map.containsKey("start")&&map.containsKey("pageSize")) {
			sb.append(" limit ?,?");
			maplist.add(map.get("start"));
			maplist.add(map.get("pageSize"));
		}
		Object[] params = maplist.toArray();
		rs=this.executeQuery(sb.toString(), params);
		List<Comments> commentList = new ArrayList<>();
		try {
			while(rs.next()) {
				Comments comment = new Comments();
				comment.setCid(rs.getInt(1));
				comment.setCnid(rs.getInt(2));
				comment.setCcontent(rs.getString(3));
				comment.setCdate(rs.getTimestamp(4));
				comment.setCip(rs.getString(5));
				comment.setCauthor(rs.getString(6));
				commentList.add(comment);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			this.closeAll();
		}
		
		return commentList;
	}

	@Override
	public List<Comments> select5() {
		String sql="select * from comments order by cdate desc limit 0,5";
		Object[] params={};
		rs=this.executeQuery(sql, params);
		List<Comments> commentList = new ArrayList<>();
		try {
			while(rs.next()) {
				Comments comment = new Comments();
				comment.setCid(rs.getInt(1));
				comment.setCnid(rs.getInt(2));
				comment.setCcontent(rs.getString(3));
				comment.setCdate(rs.getTimestamp(4));
				comment.setCip(rs.getString(5));
				comment.setCauthor(rs.getString(6));
				commentList.add(comment);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			this.closeAll();
		}
		return commentList;
	}

	@Override
	public int deleteCommentsByCnid(int cnid) {
		String sql="delete from comments where cnid =?";
		Object[] params= {cnid};
		int flag=this.executeUpdate(sql, params);
		this.closeAll();
		return flag;
	}

}
