package com.quan.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.quan.pojo.News;

public interface NewsDao {
	int selectCounts(Map<String,Object> map);
	List<News> selectByLimit(Map<String,Object> map);
	News selectByNid(int nid);
	int update(News news);
	int add(News news);
	//查询该主题下是否有新闻
	int selectByTid(int tid);
	int deleteByNid(int nid);
	int deleteNewsAndComments(int nid) throws SQLException;
}
