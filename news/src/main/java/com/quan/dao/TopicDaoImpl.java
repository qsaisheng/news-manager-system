package com.quan.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.quan.pojo.Topic;

public class TopicDaoImpl extends BaseDao implements TopicDao {

	@Override
	public List<Topic> selectAll() {
		String sql="select * from topic";
		Object[] params= {};
		rs=this.executeQuery(sql, params);
		List<Topic> topicList = new ArrayList<Topic>();
		try {
			while(rs.next()) {
				Topic topic = new Topic();
				topic.setTid(rs.getInt(1));
				topic.setTname(rs.getString(2));
				topicList.add(topic);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			this.closeAll();
		}
		return topicList;
	}

	@Override
	public Topic selectOne(int tid) {
		String sql="select * from topic where tid=?";
		Object[] params= {tid};
		rs=this.executeQuery(sql, params);
		Topic topic  = new Topic();
		try {
			while(rs.next()) {
				topic.setTid(rs.getInt(1));
				topic.setTname(rs.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			this.closeAll();
		}
		System.out.println("SelectOne-topic==>"+topic);
		return topic;
	}

	@Override
	public int addOne(String tname) {
		String sql="insert into topic(tname) values(?)";
		Object[] params= {tname};
		int flag=this.executeUpdate(sql, params);
		this.closeAll();
		return flag;
	}

	@Override
	public Topic selectOneByName(String tname) {
		String sql="select * from topic where tname=?";
		Object[] params= {tname};
		rs=this.executeQuery(sql, params);
		Topic topic = new Topic();
		topic.setTid(-1);
		try {
			while(rs.next()) {
				topic.setTid(rs.getInt(1));
				topic.setTname(rs.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			this.closeAll();
		}
		return topic;
	}

	@Override
	public int deleteByTid(int tid) {
		String sql="delete from topic where tid=?";
		Object[] param= {tid};
		int flag=this.executeUpdate(sql, param);
		this.closeAll();
		return flag;
	}

	@Override
	public int updateTopic(int tid,String tname) {
		String sql="update topic set tname=? where tid=?";
		Object[] params= {tname,tid};
		int flag=this.executeUpdate(sql, params);
		this.closeAll();
		return flag;
	}

}
