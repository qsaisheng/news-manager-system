package com.quan.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.quan.pojo.News;
import com.quan.pojo.User;

public class UserDaoImpl extends BaseDao implements UserDao{

	@Override
	public User login(String username, String password) {

		String sql="select * from news_users where uname=? and upwd=?";
		Object[] params = {username,password};
		rs=this.executeQuery(sql, params);
		User user = new User();
		try {
			if(rs.next()) {
				user.setUid(rs.getInt(1));
				user.setUname(rs.getString(2));
				user.setUpwd(rs.getString(3));
				user.setType(rs.getInt(4));
				user.setIsbanned(rs.getString(5));
			}else {
				user.setType(0);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			this.closeAll();
		}
		return user;
	}

	@Override
	public int register(String username, String password) {
		String sql="insert into news_users(uname,upwd,type) values(?,?,1)";
		Object[] params= {username,password};
		int updateRows=this.executeUpdate(sql, params);
		return updateRows;
	}

	@Override
	public int selectUsername(String username) {
		String sql="select * from news_users where uname=?";
		int flag=-1;
		Object[] params = {username};
		rs=this.executeQuery(sql, params);
		try {
			flag = rs.next()==true ? 0:1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return flag;
	}

	@Override
	public List<User> selectAll() {
		String sql="select * from news_users";
		Object[] params= {};
		rs=this.executeQuery(sql, params);
		List<User> userList= new ArrayList<User>();
		try {
			while(rs.next()) {
				User user = new User();
				user.setUid(rs.getInt(1));
				user.setUname(rs.getString(2));
				user.setUpwd(rs.getString(3));
				user.setType(rs.getInt(4));
				user.setIsbanned(rs.getString(5));
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.closeAll();
		}
		return userList;
	}

	@Override
	public User selectOne(int uid) {
		String sql="select * from news_users where uid=?";
		Object[] params= {uid};
		rs=this.executeQuery(sql, params);
		User user =new User();
		try {
			while(rs.next()) {
				user.setUid(rs.getInt(1));
				user.setUname(rs.getString(2));
				user.setUpwd(rs.getString(3));
				user.setType(rs.getInt(4));
				user.setIsbanned(rs.getString(5));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			this.closeAll();
		}
		return user;
	}

	@Override
	public int updateUser(User user) {
		String sql="update news_users set isbanned = ? ,upwd=? where uid=?";
		Object[] params= {user.getIsbanned(),user.getUpwd(),user.getUid()};
		int flag=this.executeUpdate(sql, params);
		this.closeAll();
		return flag;
	}

	@Override
	public int deleteUser(int uid) {
		String sql="delete from news_users where uid=?";
		Object[] params= {uid};
		int flag=this.executeUpdate(sql, params);
		this.closeAll();
		return flag;
	}

	@Override
	public int selectUserCounts(Map<String, Object> map) {
		StringBuffer sb= new StringBuffer();
		int flag=-1;
		sb.append("select count(1) from news_users where 1=1 and uid != 1");
		List<Object> maplist = new ArrayList<>();
		if(map.containsKey("uname")) {
			sb.append(" and uname like ?");
			maplist.add("%"+map.get("uname")+"%");
		}
		Object[] params=maplist.toArray();
		rs=this.executeQuery(sb.toString(), params);
		try {
			while(rs.next()) {
				flag=rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			this.closeAll();
		}
		return flag;
	}

	@Override
	public List<User> selectByLimit(Map<String, Object> map) {
		StringBuffer sb= new StringBuffer();
		sb.append("select * from news_users where 1=1 and uid != 1");
		List<Object> maplist = new ArrayList<>();
		if(map.containsKey("uname")) {
			sb.append(" and uname like ?");
			maplist.add("%"+map.get("uname")+"%");
		}
		if(map.containsKey("start")&&map.containsKey("pageSize")) {
			sb.append(" limit ?,?");
			maplist.add(map.get("start"));
			maplist.add(map.get("pageSize"));
		}
		Object[] params=maplist.toArray();
		System.out.println(map.toString());
		System.out.println(sb.toString());
		System.out.println(maplist.toString());
		rs = this.executeQuery(sb.toString(), params);
		List<User> userList = new ArrayList<User>();
		try {
			while(rs.next()) {
				User user = new User();
				user.setUid(rs.getInt(1));
				user.setUname(rs.getString(2));
				user.setIsbanned(rs.getString(5));
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			this.closeAll();
		}
		return userList;
	}

}
