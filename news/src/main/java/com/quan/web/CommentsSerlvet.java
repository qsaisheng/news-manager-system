package com.quan.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.quan.dao.CommentDao;
import com.quan.dao.CommentDaoImpl;
import com.quan.pojo.Comments;
import com.quan.service.CommentService;
import com.quan.service.CommentServiceImpl;
import com.quan.util.PV;
import com.quan.util.PageBean;

@WebServlet("/commentsSerlvet")
public class CommentsSerlvet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		resp.setCharacterEncoding("UTF-8");
		CommentService commentService = new CommentServiceImpl();
		List<Comments> commentsList = null;
		String start = req.getParameter("start");
		String pageSize = req.getParameter("pageSize");
		String pageNowT = req.getParameter("pageNow");
		int size = 0;
		int pageNow = 0;
		int counts = 0;
		Map<String, Object> map = new HashMap<String, Object>();
		if (pageNowT != null) {
			pageNow = Integer.parseInt(pageNowT);
		}
		if (action.equals("selectAll")) {
			String cnid = req.getParameter("cnid");
			map.put("cnid", cnid);
			if (start != null && pageSize != null) {
				if (Integer.parseInt(start) >= 0 && Integer.parseInt(pageSize) > 0) {
					map.put("start", Integer.parseInt(start));
					map.put("pageSize", Integer.parseInt(pageSize));
					size = Integer.parseInt(pageSize);
				}
			}
			counts = commentService.selectCommentsCounts(map);
			if (Integer.parseInt(start) != 0)
				pageNow =  Integer.parseInt(start)/size + 1;
			else {
				pageNow = 1;
			}
			commentsList = commentService.selectByLimit(map);
			PageBean<Comments> pageBean = new PageBean<Comments>(pageNow, size, commentsList, counts);
			resp.getWriter().write(JSON.toJSONStringWithDateFormat(pageBean, "yyyy-MM-dd hh:mm:ss"));
			resp.getWriter().close();
		} else if (action.equals("addComments")) {
			int cnid = -1;
			if (req.getParameter("cnid") != null) {
				cnid = Integer.parseInt(req.getParameter("cnid"));
			}
			String username = req.getParameter("username");
			String ccontent = req.getParameter("ccontent");
			String address = PV.getClientIpAddress(req);
			if(commentService.addComments(cnid, username, ccontent, address)) {
				resp.getWriter().write("true");
			}else {
				resp.getWriter().write("false");
			}
			resp.getWriter().close();
		}else if(action.equals("deleteOne")) {
			int cid=req.getParameter("cid")==null?-1:Integer.parseInt(req.getParameter("cid"));
			if(commentService.deleteComments(cid)) {
				resp.getWriter().write("true");
			}else {
				resp.getWriter().write("false");
			}
			resp.getWriter().close();
		}else if(action.equals("select5")) {
			commentsList=commentService.select5();
			resp.getWriter().write(JSON.toJSONString(commentsList));
			resp.getWriter().flush();
			resp.getWriter().close();
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doGet(req, resp);
	}

}
