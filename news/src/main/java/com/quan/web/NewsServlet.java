package com.quan.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.quan.dao.NewsDao;
import com.quan.dao.NewsDaoImpl;
import com.quan.pojo.News;
import com.quan.service.NewsService;
import com.quan.service.NewsServiceImpl;
@WebServlet("/newsServlet")
public class NewsServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		NewsService newsService = new NewsServiceImpl();
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		String nid=req.getParameter("nid");
		String action=req.getParameter("action");
		resp.setContentType("application/json");
		String s=null;
		System.out.println("NewsServlet-action==>"+action);
		if(action.equals("selectOne")) {
			
			NewsDao newsDao = new NewsDaoImpl();
			News news=newsService.selectByNid(Integer.parseInt(nid));
			s=JSON.toJSONStringWithDateFormat(news,"yyyy-MM-dd hh:mm:ss");
		}else if(action.equals("delete")){
			s=newsService.delete(Integer.parseInt(nid))==true?"true":"false";
		}
		resp.getWriter().write(s);
		resp.getWriter().close();

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doGet(req, resp);
	}
	
}
