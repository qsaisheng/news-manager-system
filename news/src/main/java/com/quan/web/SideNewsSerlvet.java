package com.quan.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.quan.dao.NewsDao;
import com.quan.dao.NewsDaoImpl;
import com.quan.pojo.News;
@WebServlet("/sideNewsSerlvet")
public class SideNewsSerlvet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		resp.setCharacterEncoding("utf-8");
		NewsDao newsDao = new NewsDaoImpl();
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("ntid",1);
		map.put("start", 0);
		map.put("pageSize", 5);
		List<News> list1= newsDao.selectByLimit(map);
		map.replace("ntid", 2);
		List<News> list2= newsDao.selectByLimit(map);
		map.replace("ntid", 5);
		List<News> list5= newsDao.selectByLimit(map);
		Map<String,Object> listMap = new HashMap<String,Object>();
		listMap.put("list1", list1);
		listMap.put("list2", list2);
		listMap.put("list5", list5);
		resp.setContentType("application/json");
		System.out.println("listMap==>"+JSON.toJSONString(listMap));
		resp.getWriter().write(JSON.toJSONString(listMap));
		resp.getWriter().close();
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doGet(req, resp);
	}

}
