package com.quan.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.quan.dao.NewsDao;
import com.quan.dao.NewsDaoImpl;
import com.quan.pojo.News;
import com.quan.service.NewsService;
import com.quan.service.NewsServiceImpl;
import com.quan.util.PageBean;
@WebServlet("/pageBeanServlet")
public class PageBeanServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String ntid=req.getParameter("ntid");
		String ntitle=req.getParameter("ntitle");
		String start= req.getParameter("start");
		String pageSize= req.getParameter("pageSize");
		String pageNowT = req.getParameter("pageNow");
		int size=0;
		int pageNow=0;
		if(pageNowT!=null) {
			pageNow = Integer.parseInt(pageNowT);
		}
		NewsService newsService = new NewsServiceImpl();
		Map <String,Object> map=new HashMap();
		if(ntid!=null) {
			if(Integer.parseInt(ntid)!=-1) {
				map.put("ntid", Integer.parseInt(ntid));
			}
		}
		if(ntitle!=null&&!ntitle.equals("")) {
			map.put("ntitle", ntitle);
		}
		if(start!=null&&pageSize!=null) {
			if(Integer.parseInt(start)>=0&&Integer.parseInt(pageSize)>0) {
				map.put("start", Integer.parseInt(start));
				map.put("pageSize", Integer.parseInt(pageSize));
				size=Integer.parseInt(pageSize);
			}
		}
		List<News> newsList= newsService.selectByLimit(map);
		
		int counts = newsService.selectCounts(map);
		if(Integer.parseInt(start)!=0)
			pageNow =  Integer.parseInt(start)/size+1;
		else {
			pageNow=1;
		}
		PageBean<News> pageBean = new PageBean<News>(pageNow,size,newsList,counts);
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(JSON.toJSONStringWithDateFormat(pageBean,"yyyy-MM-dd hh:mm:ss"));
		resp.getWriter().close();
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doGet(req, resp);
	}
	
}
