package com.quan.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.quan.pojo.User;
@WebServlet("/getSessionServlet")
public class GetSessionServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setCharacterEncoding("UTF-8");
		String action=req.getParameter("action");
		if(action.equals("get")) {
			User user= (User)req.getSession().getAttribute("user");
			resp.getWriter().write(JSON.toJSONString(user));
			System.out.println("user==>"+user);
			resp.getWriter().flush();
			resp.getWriter().close();
		}else if(action.equals("delete")) {
			User user= (User)req.getSession().getAttribute("user");
			if(user!=null) {
				System.out.println("user==>"+user);
				req.getSession().removeAttribute("user");
			}
			
			resp.sendRedirect("/news/index.html");
		}

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doPost(req, resp);
	}

}
