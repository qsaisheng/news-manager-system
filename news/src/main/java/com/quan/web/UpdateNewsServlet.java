package com.quan.web;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.quan.dao.NewsDao;
import com.quan.dao.NewsDaoImpl;
import com.quan.pojo.News;

@WebServlet("/updateNewsServlet")
public class UpdateNewsServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		boolean isFile = ServletFileUpload.isMultipartContent(req);
		String newFileName = null;
		String newsTitle = null;
		String newsAuthor = null;
		String newsSummary=null;
		String newsContent=null;
		String npicpath=null;
		String action=null;
		String path=null;
		int ntid=-1;
		int nid=-1;
		News news=new News();
		long time=System.currentTimeMillis();
		news.setNmodifyDate(new Timestamp(time));
		NewsDao newsDao = new NewsDaoImpl();
		System.out.println("isFile" + isFile);
		if (isFile) {
			FileItemFactory fileItemFactory = new DiskFileItemFactory();
			ServletFileUpload sfu = new ServletFileUpload(fileItemFactory);
			try {
				List<FileItem> list = sfu.parseRequest(req);
				for (FileItem fileItem : list) {
					if (fileItem.isFormField()) {
						if (fileItem.getFieldName().equals("newsAuthor")) {
							newsAuthor = fileItem.getString("UTF-8");
							news.setNauthor(newsAuthor);
						}else if(fileItem.getFieldName().equals("newsTitle")) {
							newsTitle = fileItem.getString("UTF-8");
							news.setNtitle(newsTitle);
						}else if(fileItem.getFieldName().equals("newsSummary")) {
							newsSummary = fileItem.getString("UTF-8");
							news.setNsummary(newsSummary);
						}else if(fileItem.getFieldName().equals("newsContent")) {
							newsContent = fileItem.getString("UTF-8");
							news.setNcontent(newsContent);
						}else if(fileItem.getFieldName().equals("ntid")) {
							ntid= Integer.parseInt(fileItem.getString("UTF-8"));
							news.setNtid(ntid);
						}else if(fileItem.getFieldName().equals("nid")) {
							nid= Integer.parseInt(fileItem.getString("UTF-8"));
							news.setNid(nid);
						}else if(fileItem.getFieldName().equals("action")) {
							action=fileItem.getString("UTF-8");
							path=req.getServletContext().getRealPath("/upload");
						}
					} else {
						String fileName = fileItem.getName();
						if(fileName!=null) {
							int temp = fileName.lastIndexOf(".");
							newFileName = UUID.randomUUID().toString() + fileName.substring(temp, fileName.length());
							path = req.getServletContext().getRealPath("/upload");
							news.setNpicPath(newFileName);
							File file = new File(path);
							if (!file.exists()) {
								file.mkdirs();
							}
							fileItem.write(new File(path, newFileName));
						}else {
							System.out.println("δ�ϴ��ļ�");
						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		int i=-1;
		if(action.equals("update")) {
			
			//ɾ����ͼƬ
			News news1 =newsDao.selectByNid(news.getNid());
			String path1=req.getServletContext().getRealPath("/upload")+"/"+news1.getNpicPath();
			System.out.println(path1);
			File file1 = new File(path1);
			if(file1.exists()) {
				file1.delete();
			}
			i=newsDao.update(news);
		}else if(action.equals("add")) {
			news.setNcreateDate(new Timestamp(time));
			i=newsDao.add(news);
		}
		resp.sendRedirect("/news/adminPage/admin.html");
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doGet(req, resp);
	}

}
