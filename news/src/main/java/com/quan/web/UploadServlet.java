package com.quan.web;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.alibaba.fastjson.JSON;
@WebServlet("/uploadServlet")
public class UploadServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		boolean isFile = ServletFileUpload.isMultipartContent(req);
		String username=null;
		String newFileName=null;
		System.out.println("isFile"+isFile);
		if(isFile) {
			FileItemFactory fileItemFactory= new DiskFileItemFactory();
			ServletFileUpload sfu=new ServletFileUpload(fileItemFactory);
			try {
				List<FileItem> list= sfu.parseRequest(req);
				for(FileItem fileItem:list) {
					if(fileItem.isFormField()) {
						if(fileItem.getFieldName().equals("username")) {
							username=fileItem.getString("UTF-8");
							System.out.println("UploadServlet-username==>"+username);
						}
					}else {
						String fileName=fileItem.getName();
						int temp=fileName.lastIndexOf("."); 
						newFileName = UUID.randomUUID().toString()+fileName.substring(temp,fileName.length());
						System.out.println("UploadServlet-newFileName==>"+newFileName);
						String path=req.getServletContext().getRealPath("/upload");
						File file=new File(path);
						if(!file.exists()) {
							file.mkdirs();
						}
						fileItem.write(new File(path,newFileName));
					}
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		req.setAttribute("username", username);
		req.setAttribute("newFileName", newFileName);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doGet(req, resp);
	}
	
}
