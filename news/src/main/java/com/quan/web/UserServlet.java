package com.quan.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.quan.dao.UserDao;
import com.quan.dao.UserDaoImpl;
import com.quan.pojo.News;
import com.quan.pojo.User;
import com.quan.service.UserService;
import com.quan.service.UserServiceImpl;
import com.quan.util.PV;
import com.quan.util.PageBean;
@WebServlet("/userServlet")
public class UserServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		UserService userService = new UserServiceImpl();
		String data = null;
		resp.setCharacterEncoding("utf-8");
		String username=req.getParameter("uname");
		String password = req.getParameter("upwd");
		String action=req.getParameter("action");
		if(action.equals("login")) {
			User user = userService.login(username, password);
			req.getSession().setAttribute("user", user);
			data=JSON.toJSONString(user);
		}else if(action.equals("register")) {
				data=userService.register(username, password)==true?"true":"false";
				resp.getWriter().write("<html><head><meta charset='utf-8'/></head><body><script>alert('ע��ɹ�');window.location.href='/news/index.html'</script></body><html>");
		}else if(action.equals("logout")) {
			if(req.getSession().getAttribute("user")!=null) {
				req.getSession().removeAttribute("user");
			}
		}else if(action.equals("selectAll")) {
			String uname = req.getParameter("uname");
			System.out.println(uname);
			String start = req.getParameter("start");
			String pageSize = req.getParameter("pageSize");
		
			Map<String, Object> map = new HashMap<String,Object>();
			if(uname!=null&&!uname.equals("")) {
				map.put("uname", uname);
			}
			if(start!=null&&pageSize!=null) {
				map.put("start", Integer.parseInt(start));
				map.put("pageSize",Integer.parseInt(pageSize));
			}
			int counts=userService.selectUserCounts(map);
			int pageNow;
			List<User> userList = userService.selectByLimit(map);
			if(Integer.parseInt(start)!=0) {
				pageNow=Integer.parseInt(start)/Integer.parseInt(pageSize)+1;
			}else {
				pageNow=1;
			}
			PageBean<User> pageBean = new PageBean<User>(pageNow,Integer.parseInt(pageSize),userList,counts);
			data=JSON.toJSONString(pageBean);
		}else if(action.equals("ban")) {
			int uid=Integer.parseInt(req.getParameter("uid"));
			User user=userService.selectOne(uid);
			user.setIsbanned("����");
			data="true";
			userService.updateUser(user);
		}else if(action.equals("release")) {
			int uid=Integer.parseInt(req.getParameter("uid"));
			User user=userService.selectOne(uid);
			user.setIsbanned("����");
			userService.updateUser(user);
			data="true";
		}else if(action.equals("reset")) {
			int uid=Integer.parseInt(req.getParameter("uid"));
			User user=userService.selectOne(uid);
			user.setUpwd("123456");
			userService.updateUser(user);
			data="true";
		}else if(action.equals("delete")) {
			int uid=Integer.parseInt(req.getParameter("uid"));
			userService.deleteUser(uid);
			data="true";
		}else if(action.equals("selectUname")) {
			if(!userService.selectUsername(username)) {
				data="true";
			}else {
				data="false";
			}
		}
		resp.getWriter().write(data);
		resp.getWriter().flush();
		resp.getWriter().close();
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doGet(req, resp);
	}
	
}
