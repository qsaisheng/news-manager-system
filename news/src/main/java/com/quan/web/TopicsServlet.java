package com.quan.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.quan.dao.TopicDao;
import com.quan.dao.TopicDaoImpl;
import com.quan.pojo.Topic;
import com.quan.service.NewsService;
import com.quan.service.NewsServiceImpl;
import com.quan.service.TopicService;
import com.quan.service.TopicServiceImpl;

@WebServlet("/topicsServlet")
public class TopicsServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		resp.setCharacterEncoding("utf-8");
		TopicService topicService = new TopicServiceImpl();
		NewsService newsService = new NewsServiceImpl();
		if (action.equals("selectAll")) {
			List<Topic> topicList = topicService.selectAll();
			resp.setContentType("application/json");
			resp.getWriter().write(JSON.toJSONString(topicList));
			resp.getWriter().close();
		} else if (action.equals("selectOne")) {
			int ntid = -1;
			if (req.getParameter("ntid") != null) {
				ntid = Integer.parseInt(req.getParameter("ntid"));
			}
			Topic topic = topicService.selectOne(ntid);
			String s = JSON.toJSONString(topic);
			resp.getWriter().write(s);
			resp.getWriter().close();
		} else if (action.equals("addOne")) {
			String tname = req.getParameter("tname");
			if(topicService.addOne(tname)) {
				resp.sendRedirect("/news/adminPage/admin.html");
			}
		}else if(action.equals("selectOneByName")) {
			String tname = req.getParameter("tname");
			Topic topic=topicService.selectOneByName(tname);
			if(topic.getTid()!=-1) {
				resp.getWriter().write("false");
			}else {
				resp.getWriter().write("true");
			}
		}else if(action.equals("deleteOneByTid")) {
			String tid=req.getParameter("tid");
			String flag=null;
			if(tid!=null) {
				int ntid=Integer.parseInt(tid);
				if(newsService.selectByTid(ntid)) {
					flag="true";
				}else {
					topicService.deleteByTid(ntid);
					flag="false";
				}
				resp.getWriter().write(flag);
				resp.getWriter().flush();
				resp.getWriter().close();
			}
		}else if(action.equals("updateTopic")) {
			int tid = Integer.parseInt(req.getParameter("tid"));
			String tname=req.getParameter("tname");
			topicService.updateTopic(tid, tname);
		}else if(action.equals("selectRepeat")) {
			String tname=req.getParameter("tname");
			//Topic topic= topicService.selectOne(Integer.parseInt(tid));
			Topic topic1=topicService.selectOneByName(tname);
			System.out.println("selectOne");
			if(topic1.getTid()!=-1) {
				System.out.println("selectOne-false");
				resp.getWriter().write("false");
			}else {
				System.out.println("selectOne-true");
				resp.getWriter().write("true");
			}
			resp.getWriter().flush();
			resp.getWriter().close();
		}

	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doGet(req, resp);
	}

}
