package com.quan.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quan.pojo.User;
@WebFilter("/*")
public class MyFilter implements Filter{

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filter)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		String url=request.getRequestURI();
		 response.setHeader("Pragma","no-cache");    
         response.setHeader("Cache-Control","no-cache");    
         response.setDateHeader("Expires", -1);  
		if(url.contains("adminPage")) {;
			User user=(User)request.getSession().getAttribute("user");
			if(user!=null&&user.getType()==-1) {
				filter.doFilter(req, resp);
				System.out.println("����IF");

			}else {
				response.sendRedirect("/news/index.html");

			}
		}else {
			filter.doFilter(req, resp);

		}
	}
	
}
