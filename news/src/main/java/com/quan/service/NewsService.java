package com.quan.service;

import java.util.List;
import java.util.Map;

import com.quan.pojo.News;

public interface NewsService {
	int selectCounts(Map<String,Object> map);
	List<News> selectByLimit(Map<String,Object> map);
	News selectByNid(int nid);
	boolean update(News news);
	boolean add(News news);
	//查询该主题下是否有新闻
	boolean selectByTid(int tid);
	//删除新闻
	boolean delete(int nid);
}
