package com.quan.service;

import java.util.List;
import java.util.Map;

import com.quan.pojo.User;

public interface UserService {
	User login(String username,String password);
	boolean register(String username,String password);
	//查询注册是否用户名重复
	boolean selectUsername(String username);
	List<User> selectAll();
	User selectOne(int uid);
	boolean updateUser(User user);
	boolean deleteUser(int uid);
	int selectUserCounts(Map<String, Object> map);
	List<User> selectByLimit(Map<String,Object> map);
}
