package com.quan.service;

import java.util.List;

import com.quan.dao.TopicDao;
import com.quan.dao.TopicDaoImpl;
import com.quan.pojo.Topic;

public class TopicServiceImpl implements TopicService{

	private TopicDao topicDao;
	public TopicServiceImpl() {
		this.topicDao = new TopicDaoImpl();
	}
	@Override
	public List<Topic> selectAll() {
		return topicDao.selectAll();
	}

	@Override
	public Topic selectOne(int tid) {
		return topicDao.selectOne(tid);
	}

	@Override
	public boolean addOne(String tname) {
		return topicDao.addOne(tname)>0?true:false;
	}

	@Override
	public Topic selectOneByName(String tname) {
		return topicDao.selectOneByName(tname);
	}

	@Override
	public boolean deleteByTid(int tid) {
		return topicDao.deleteByTid(tid)>0?true:false;
	}
	@Override
	public boolean updateTopic(int tid, String tname) {
		return topicDao.updateTopic(tid, tname)>0?true:false;
	}

}
