package com.quan.service;

import java.util.List;
import java.util.Map;

import com.quan.pojo.Comments;

public interface CommentService {
	//查询当前新闻评论的数量
	int selectCommentsCounts(Map<String,Object> map);
	//查询当前新闻的所有评论
	List<Comments> selectAllCommentsByCnid(int cnid);
	//添加评论
	boolean addComments(int cnid,String cauthor,String ccontent,String cip);
	//删除评论
	boolean deleteComments(int cid);
	List<Comments> selectByLimit(Map<String,Object> map);
	List<Comments> select5();
}
