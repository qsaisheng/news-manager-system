package com.quan.service;

import java.util.List;

import com.quan.pojo.Topic;

public interface TopicService {
	List<Topic> selectAll();
	Topic selectOne(int tid);
	boolean addOne(String tname);
	Topic selectOneByName(String tname);
	boolean deleteByTid(int tid);
	boolean updateTopic(int tid,String tname);
}
