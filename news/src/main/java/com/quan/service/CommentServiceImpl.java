package com.quan.service;

import java.util.List;
import java.util.Map;

import com.quan.dao.CommentDao;
import com.quan.dao.CommentDaoImpl;
import com.quan.pojo.Comments;

public class CommentServiceImpl implements CommentService{

	private CommentDao commentDao;
	public CommentServiceImpl() {
		this.commentDao = new CommentDaoImpl();
	}
	@Override
	public int selectCommentsCounts(Map<String,Object> map) {
		return commentDao.selectCommentsCounts(map);
	}

	@Override
	public List<Comments> selectAllCommentsByCnid(int cnid) {
		return commentDao.selectAllCommentsByCnid(cnid);
	}

	@Override
	public boolean addComments(int cnid, String cauthor, String ccontent, String cip) {
		return commentDao.addComments(cnid, cauthor, ccontent, cip)>0?true:false;
	}

	@Override
	public boolean deleteComments(int cid) {
		return commentDao.deleteComments(cid)>0?true:false;
	}

	@Override
	public List<Comments> selectByLimit(Map<String,Object> map) {
		return commentDao.selectByLimit(map);
	}
	@Override
	public List<Comments> select5() {
		return commentDao.select5();
	}
	
}
