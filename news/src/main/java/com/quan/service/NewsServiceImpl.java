package com.quan.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.quan.dao.CommentDao;
import com.quan.dao.CommentDaoImpl;
import com.quan.dao.NewsDao;
import com.quan.dao.NewsDaoImpl;
import com.quan.pojo.News;

public class NewsServiceImpl implements NewsService{

	private NewsDao newsDao;
	private CommentDao commentDao;
	public NewsServiceImpl() {
		this.newsDao = new NewsDaoImpl();
		this.commentDao= new CommentDaoImpl();
	}
	@Override
	public int selectCounts(Map<String, Object> map) {
		return newsDao.selectCounts(map);
	}

	@Override
	public List<News> selectByLimit(Map<String, Object> map) {
		return newsDao.selectByLimit(map);
	}

	@Override
	public News selectByNid(int nid) {
		return newsDao.selectByNid(nid);
	}

	@Override
	public boolean update(News news) {
		return newsDao.update(news)>0?true:false;
	}

	@Override
	public boolean add(News news) {
		return newsDao.add(news)>0?true:false;
	}

	@Override
	public boolean selectByTid(int tid) {
		return newsDao.selectByTid(tid)>0 ? true:false;
	}
	@Override
	public boolean delete(int nid) {
		boolean flag=false;
		try {
			flag= newsDao.deleteNewsAndComments(nid)>0?true:false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}
	
}
