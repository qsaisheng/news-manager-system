package com.quan.service;

import java.util.List;
import java.util.Map;

import com.quan.dao.UserDao;
import com.quan.dao.UserDaoImpl;
import com.quan.pojo.User;

public class UserServiceImpl implements UserService{

	private UserDao userDao;
	public UserServiceImpl() {
		this.userDao=new UserDaoImpl();
	}
	@Override
	public User login(String username, String password) {
		return userDao.login(username, password);
	}

	@Override
	public boolean register(String username, String password) {
		return userDao.register(username, password)>0?true:false;
	}

	@Override
	public boolean selectUsername(String username) {
		return userDao.selectUsername(username)>0 ?true:false;
	}
	@Override
	public List<User> selectAll() {
		return userDao.selectAll();
	}
	@Override
	public User selectOne(int uid) {
		return userDao.selectOne(uid);
	}
	@Override
	public boolean updateUser(User user) {
		return userDao.updateUser(user)>0?true:false;
	}
	@Override
	public boolean deleteUser(int uid) {
		return userDao.deleteUser(uid)>0?true:false;
	}
	@Override
	public int selectUserCounts(Map<String, Object> map) {
		return userDao.selectUserCounts(map);
	}
	@Override
	public List<User> selectByLimit(Map<String, Object> map) {
		return userDao.selectByLimit(map);
	}

}
