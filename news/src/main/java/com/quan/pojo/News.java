package com.quan.pojo;

import java.sql.Timestamp;
import java.util.Date;

public class News {
	private Integer nid;
	private Integer ntid;
	private String ntitle;
	private String nauthor;
	private Timestamp  ncreateDate;
	private String npicPath;
	private String ncontent;
	private Timestamp nmodifyDate;
	private String nsummary;
	public Integer getNid() {
		return nid;
	}
	public void setNid(Integer nid) {
		this.nid = nid;
	}
	public Integer getNtid() {
		return ntid;
	}
	public void setNtid(Integer ntid) {
		this.ntid = ntid;
	}
	public String getNtitle() {
		return ntitle;
	}
	public void setNtitle(String ntitle) {
		this.ntitle = ntitle;
	}
	public String getNauthor() {
		return nauthor;
	}
	public void setNauthor(String nauthor) {
		this.nauthor = nauthor;
	}
	public Timestamp getNcreateDate() {
		return ncreateDate;
	}
	public void setNcreateDate(Timestamp ncreateDate) {
		this.ncreateDate = ncreateDate;
	}
	public String getNpicPath() {
		return npicPath;
	}
	public void setNpicPath(String npicPath) {
		this.npicPath = npicPath;
	}
	public String getNcontent() {
		return ncontent;
	}
	public void setNcontent(String ncontent) {
		this.ncontent = ncontent;
	}
	public Timestamp getNmodifyDate() {
		return nmodifyDate;
	}
	public void setNmodifyDate(Timestamp nmodifyDate) {
		this.nmodifyDate = nmodifyDate;
	}
	public String getNsummary() {
		return nsummary;
	}
	public void setNsummary(String nsummary) {
		this.nsummary = nsummary;
	}

}
