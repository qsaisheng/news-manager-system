package com.quan.pojo;

import java.sql.Timestamp;
import java.util.Date;
public class Comments {
	private Integer cid;
	private Integer cnid;
	private String ccontent;
	private Timestamp cdate;
	private String cip;
	private String cauthor;
	
	public Timestamp getCdate() {
		return cdate;
	}
	public void setCdate(Timestamp cdate) {
		this.cdate = cdate;
	}
	public Integer getCid() {
		return cid;
	}
	public void setCid(Integer cid) {
		this.cid = cid;
	}
	public Integer getCnid() {
		return cnid;
	}
	public void setCnid(Integer cnid) {
		this.cnid = cnid;
	}
	public String getCcontent() {
		return ccontent;
	}
	public void setCcontent(String ccontent) {
		this.ccontent = ccontent;
	}

	public String getCip() {
		return cip;
	}
	public void setCip(String cip) {
		this.cip = cip;
	}
	public String getCauthor() {
		return cauthor;
	}
	public void setCauthor(String cauthor) {
		this.cauthor = cauthor;
	}

}
