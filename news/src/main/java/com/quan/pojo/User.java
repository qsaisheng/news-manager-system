package com.quan.pojo;

public class User {
	private Integer uid;
	private String uname;
	private String upwd;
	private Integer type;
	private String isbanned;

	public String getIsbanned() {
		return isbanned;
	}

	public void setIsbanned(String isbanned) {
		this.isbanned = isbanned;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getUpwd() {
		return upwd;
	}

	public void setUpwd(String upwd) {
		this.upwd = upwd;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "User [uid=" + uid + ", uname=" + uname + ", upwd=" + upwd + ", type=" + type + "]";
	}

}
