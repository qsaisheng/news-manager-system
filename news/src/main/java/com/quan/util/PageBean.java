package com.quan.util;

import java.util.List;

public class PageBean<T> {
	private int pageNow;
	private int pageSize;
	private int pages;
	private int counts;
	private List<T> lists;

	public PageBean() {
		super();
	}
	public PageBean(int pageNow,int pageSize ,List<T> lists,int counts) {
		super();
		this.pageNow=pageNow;
		this.pageSize=pageSize;
		this.lists=lists;
		this.counts=counts;
		this.pages = counts%pageSize==0?counts/pageSize:counts/pageSize+1;
	}
	public int getPageNow() {
		return pageNow;
	}
	public void setPageNow(int pageNow) {
		this.pageNow = pageNow;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPages() {
		return pages;
	}
	public void setPages() {
		this.pages = counts%pageSize==0? counts/pageSize:counts/pageSize+1;
	}
	public int getCounts() {
		return counts;
	}
	public void setCounts() {
		this.counts = lists.size();
	}
	public List<T> getLists() {
		return lists;
	}
	public void setLists(List<T> lists) {
		this.lists = lists;
	}
	
}
