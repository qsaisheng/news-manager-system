/**
 * 
 */
$(function(){
	//加载静态资源页面
		$(".sidebar").load("index-elements/index_sidebar.html");
		$("#rightBar").load("index-elements/index_rightbar.html");
		$("#indexBottom").load("index-elements/index_bottom.html");
		var start=0;
		var pageSize=23;
		var pagesBean;
		var ntid=-1;
		var ntitle;
		//分页切换
		$(".pageBar").click(function(){
			var action=$(this).data("type");
			if(action=="head"){
				start=0;
				pageBean();
			}else if(action=="pre"){
				start=start < pageSize? 0:start-pageSize;
				pageBean();
			}else if(action=="next"){
				start = (start+pageSize)>=pagesBean.counts?start:start+pageSize;
				pageBean();
			}else if(action=="end"){
				start=pagesBean.counts%pageSize==0?start=pagesBean.counts-pageSize:pagesBean.counts-pagesBean.counts%pageSize;
				pageBean();
			}
		});
		pageBean();
		getTopics();
		checkLogin();
		sideComment();
		sideNews();
		
		$("#search").click(function(){
			ntitle=$("#input_title").val();
			start=0;
			pageBean();
		});
		/*
			获取中间页面news
		 */
		function pageBean(){
			var rs="";
			var rs2="";
			$.ajax({
				url:"/news/pageBeanServlet",
				data:{
					"start":start,
					"pageSize":pageSize,
					"ntid":ntid,
					"ntitle":ntitle
				},
				success:function(data){
					
					pagesBean=data;
					var list = data.lists;
					if(list.length==0){
						$(".classlist").empty();
						alert("当前条件下没有新闻");
						$("#span1").html("");
					}else{
						for(var i=0 ;i< list.length ;i++){
	                  	rs+='<li>';
	                    rs+='<a href="/news/newsPages/newsRead.html?nid=';
	                    rs+=list[i].nid;
	                    rs+='">';
	                    rs+=list[i].ntitle;
	                    rs+='</a>';
						rs+="<span>"+list[i].nmodifyDate+"</span></li>"
	                    rs+='</li>';
						}
						$(".classlist").empty();
						$(".classlist").html(rs);
						$("#span1").empty();
						rs2 += "当前页数:["+data.pageNow+"/"+data.pages+"]";
						$("#span1").html(rs2);
					}

				}
			})
	
		}
		
		/*获取所有标题 */
		function getTopics(){
			var rs="";
			$.ajax({
				url:"/news/topicsServlet",
				data:{"action":"selectAll"},
				success:function(data){
					rs+="<li id='class_month'><a href='javascript:void(0)' class='newstype' data-type=";
					rs+=-1;
					rs+=">"+"全部"+"</a></li>";
					for(var i=0;i<data.length;i++){
						rs+="<li id='class_month'><a href='javascript:void(0)' class='newstype' data-type=";
						rs+=data[i].tid;
						rs+=">"+data[i].tname+"</a></li>";
					}
					$(".class_date").empty();
					$(".class_date").html(rs);
					$(".newstype").click(function(){
					ntid= $(this).data("type");
					start=0;
					ntitle="";
					pageBean();
					});
				}
			})
		};
		
		//获取左栏的新闻
		function sideNews(){
			$.ajax({
				url:"/news/sideNewsSerlvet",
				success:function(data){
					var rs1="";
		            var rs2="";
		            var rs5="";
		            var list1=data.list1;
		            var list2=data.list2;
		            var list5=data.list5;
		            for (let i = 0; i < list1.length; i++) {
		                rs1+='<li>';
		                rs1+='<a href="/news/newsPages/newsRead.html?nid=';
		                rs1+=list1[i].nid;
		                rs1+='">';
		                rs1+=list1[i].ntitle;
		                rs1+='</a>';
		                rs1+='</li>';
		            }
		            for (let i = 0; i < list2.length; i++) {
		                rs2+='<li>';
		                rs2+='<a href="/news/newsPages/newsRead.html?nid=';
		                rs2+=list2[i].nid;
		                rs2+='">';
		                rs2+=list2[i].ntitle;
		                rs2+='</a>';
		                rs2+='</li>';
		            }
		            for (let i = 0; i < list5.length; i++) {
		                rs5+='<li>';
		                rs5+='<a href="/news/newsPages/newsRead.html?nid=';
		                rs5+=list5[i].nid;
		                rs5+='">';
		                rs5+=list5[i].ntitle;
		                rs5+='</a>';
		                rs5+='</li>';
		            }
		            $("#list1").empty();
		            $("#list2").empty();
		            $("#list5").empty();
		            $("#list1").html(rs1);
		            $("#list2").html(rs2);
		            $("#list5").html(rs5);
				}
			})
		};
		
		//设置login事件
		$("#login").click(function (){
			if(($("#username_input").val())!=""&&$("#password_input").val()!=""){
				$.ajax({
					url:"/news/userServlet",
					dataType:"json",
					data:$("form").serialize(),
					success:function(data){
						var rs= "";
						if(data.type==1){
							rs+="欢迎:"+data.uname;
							rs+="<button id='logout' type='button' class='login_sub'>注销</button>";
							$("#top_login").empty();
							$("#top_login").html(rs);
							$("#logout").click(function(){
								$.ajax({
									url:"/news/getSessionServlet",
									data:{"action":"delete"},
									success:function(){
										window.location.href="/news/index.html";
									}
								})
								
							});
							sessionStorage.setItem("uname",data.uname);
							}else if(data.type==-1){
								rs+="欢迎:"+data.uname;
							    rs+="<button id='logout' type='button' class='login_sub'>注销</button>";
								$("#logout").click(function(){
								$.ajax({
									url:"/news/getSessionServlet",
									data:{"action":"delete"},
									success:function(){
										window.location.href="/news/index.html";
									}
								})
								
							});
								$("#top_login").html(rs);
								var rs3="<a href='/news/adminPage/admin.html'>进入后台</a>"
								$("#top_login").append($(rs3));
								sessionStorage.setItem("uname",data.uname);
								/*临时使用*/
								location.reload();
							}else{
								$("#error").html("用户名或密码错误").css("color","red");
						}
					}			
				})
			}else{
				$("#error").html("用户名或密码不能为空").css("color","red");
			}

		});
		
		
		//进入页面时执行,检查是否登录
		function checkLogin(){
			var username = sessionStorage.getItem("uname");
			rs="";
			$.ajax({
				url:"/news/getSessionServlet",
				data:{"action":"get"},
				dataType:"json",
				success:function(data){
					if(data.uname!=""){
						rs+="欢迎:"+username;
						rs+="<button id='logout' type='button' class='login_sub'>注销</button>";
						if(username=="admin"){
							rs+="<a href='/news/adminPage/admin.html'>进入后台</a>"
						}
						$("#top_login").html(rs);
						$("#logout").click(function(){
							$.ajax({
								url:"/news/getSessionServlet",
								data:{"action":"delete"},
								success:function(){
									window.location.href="/news/index.html";
								}
							})
						});
					}
				}
				
			})
		}
	//左部最新新闻
	function sideNews(){
		var rs="";
		var rs2="";
		$.ajax({
			url:"/news/pageBeanServlet",
			data:{
				"start":0,
				"pageSize":5,
				"ntid":-1
			},
			success:function(data){
				pagesBean=data;
				var list = data.lists;
				for(var i=0 ;i< list.length ;i++){
              	rs+='<li>';
                rs+='<a href="/news/newsPages/newsRead.html?nid=';
                rs+=list[i].nid;
                rs+='">';
                rs+=list[i].ntitle;
                rs+='</a>';
                rs+='</li>';
				}
				$("#list1").empty();
				$("#list1").html(rs);
			}
		})
	}
	
	//左部最新评论
	function sideComment(){
		var rs="";
		$.ajax({
			url:"/news/commentsSerlvet",
			data:{"action":"select5"},
			dataType:"json",
			success:function(data){
				for(var i=0 ;i< data.length ;i++){
	              	rs+='<li>';
	                rs+=data[i].ccontent;
	                rs+='</li>';
				}
				$("#list2").html(rs);
			}
		})
	}
})