/**
 * 
 */
$(function(){
		var start=0;
		var pageSize=33;
		var pagesBean;
		var ntid=-1;
		
		//分页切换
		$(".pageBar").click(function(){
			var action=$(this).data("type");
			if(action=="head"){
				start=0;
				pageBean();
			}else if(action=="pre"){
				start=start < pageSize? 0:start-pageSize;
				pageBean();
			}else if(action=="next"){
				start = (start+pageSize)>pagesBean.counts?start:start+pageSize;
				pageBean();
			}else if(action=="end"){
				start=pagesBean.counts%pageSize==0?start=pagesBean.counts-pageSize:pagesBean.counts-pagesBean.counts%pageSize;
				pageBean();
			}
		});
		
		$(document).on('click','.delete',function(){
			var nid = $(this).data("nid");
			var flag=confirm("确认删除? 该操作不可逆且会删除该新闻下的评论");
			if(flag){
				$.ajax({
				url:"/news/newsServlet",
				data:{"action":"delete","nid":nid},
				success:function(data){
						alert("删除成功");
						pageBean();
				}
			})
		}

		});
		pageBean();
	
	$("#logout").click(function(){
		$.ajax({
			url:"/news/getSessionServlet",
			data:{"action":"delete"},
		})
		window.location.href="/news/index.html";
		sessionStorage.removeItem("uname",data.uname);
	})
	
	//获取所有新闻
	function pageBean(){
			var rs="";
			var rs2="";
			$.ajax({
				url:"/news/pageBeanServlet",
				data:{
					"start":start,
					"pageSize":pageSize,
					"ntid":ntid
				},
				success:function(data){
					pagesBean=data;
					var list = data.lists;
					for(var i=0 ;i< list.length ;i++){
                  	rs+='<li>';
                    rs+='<a href="/news/adminPage/newsModify.html?nid=';
                    rs+=list[i].nid;
                    rs+='">';
                    rs+=list[i].ntitle;
                    rs+='</a>';
					rs+="<span>"+list[i].nmodifyDate+"</span>";
					rs+="<a style='float:right;color:blue' class='delete' href='javascript:void(0)' data-nid='"+list[i].nid+"'>删除</a>";
                    rs+='</li>';
					}
					$(".classlist").empty();
					$(".classlist").html(rs);
					$("#span1").empty();
					rs2 += "pageNow:["+data.pageNow+"/"+data.pages+"]";
					$("#span1").html(rs2);
				}
			})
	}
})
