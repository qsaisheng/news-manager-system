/**
 * 
 */
$(function(){

	var start=0;
	var pageSize=3;
	var pagesBean;
	var name;
	var tid;
	checkLogin();
	readNews();
	getComments();
	$(".pageBar").click(function(){
			var action=$(this).data("type");
			if(action=="head"){
				start=0;
				getComments()
			}else if(action=="pre"){
				start=start < pageSize? 0:start-pageSize;
				getComments()
			}else if(action=="next"){
				start = (start+pageSize)>=pagesBean.counts?start:start+pageSize;
				getComments()
			}else if(action=="end"){
				start=pagesBean.counts%pageSize==0?start=pagesBean.counts-pageSize:pagesBean.counts-pagesBean.counts%pageSize;
				getComments()
			}
		});
	function checkLogin(){
			var username = sessionStorage.getItem("uname");
			rs="";
			$.ajax({
				url:"/news/getSessionServlet",
				data:{"action":"get"},
				dataType:"json",
				success:function(data){
					if(data.uname!=""){
						rs+="欢迎:"+username;
						rs+="<button id='logout' type='button' class='login_sub'>注销</button>";
						if(username=="admin"){
							rs+="<a href='/news/adminPage/admin.html'>进入后台</a>"
						}
						$("#top_login").html(rs);
						$("#logout").click(function(){
							$.ajax({
								url:"/news/getSessionServlet",
								data:{"action":"delete"},
								success:function(){
									window.location.href="/news/index.html";
								}
							})
						});
					}
				}
				
			})
	}
	$("#logout").click(function(){
		$.ajax({
			url:"/news/getSessionServlet",
			data:{"action":"delete"},
		})
		window.location.href="/news/index.html";
		sessionStorage.removeItem("uname",data.uname);
	})
	$(document).on('click','.delete',function(){
			var cid=$(this).data("cid");
			$.ajax({
				url:"/news/commentsSerlvet",
				data:{"action":"deleteOne","cid":cid},
				success:function(){
					getComments();
				}
			})
	});
	//注销事件
	$("document").on('click','#logout',function(){
			sessionStorage.removeItem("uname");
			window.location.href="/news/index.html";
	});
	function readNews(){
			$.ajax({
				url:"/news/newsServlet",
				async:false,
				data:{"nid":getQueryVariable("nid"),"action":"selectOne"},
				success:function(data1){
					var rs="";
					tid=data1.ntid
					document.getElementById("npicpath").value=data1.npicPath;
					document.getElementById("newsTitle").value=data1.ntitle;
					document.getElementById("newsAuthor").value=data1.nauthor;
					document.getElementById("nid").value=data1.nid;
					document.getElementById("image").src="/news/upload/"+data1.npicPath;
					$("#newsSummary").html(data1.nsummary);
					$("#newsContent").html(data1.ncontent);
					$.ajax({
						url:"/news/topicsServlet",
						dataType:"json",
						data:{"action":"selectOne","ntid":tid},
						success:function(data2){
							name=data2.tname;
							rs+="<option value='"+tid+"'>"+name+"</option>"
							$("#ntid").html(rs);
							getTopics();
						}
					})
				}
			})
		}
		//获取url的参数值
	   function getQueryVariable(variable){
	       var query = window.location.search.substring(1);
	       var vars = query.split("&");
	       for (var i=0;i<vars.length;i++) {
	               var pair = vars[i].split("=");
	               if(pair[0] == variable){return pair[1];}
	       }
	       return(false);
	   };
	   function getTopics(){
			var rs="<option value='"+tid+"'>"+name+"</option>";
			$.ajax({
				url:"/news/topicsServlet",
				data:{"action":"selectAll"},
				success:function(data){
					for(var i=0;i<data.length;i++){
						rs+="<option value='"+data[i].tid+"'>"+data[i].tname+"</option>";
					}
					$("#ntid").html(rs);
				}
			})
	   }
	
		   function getComments(){
			$.ajax({
				url:"/news/commentsSerlvet",
				dataType:"json",
				data:{"action":"selectAll",	"start":0,"pageSize":1000,"cnid":getQueryVariable("nid")},
				success:function(data){
					var list=data.lists;
					var rs="";
					for(var i=0;i<list.length;i++){
						rs+="<tr>";
						rs+="<td>留言人:</td>";
						rs+="<td id='name'>"+list[i].cauthor+"</td>";
						rs+="<td>ip:"+list[i].cip+"</td>";
						rs+="<td>留言时间:</td>";
						rs+="<td id='time'>"+list[i].cdate+"</td>";
						rs+="<td>";
						rs+="<a href='javascript:void(0)' class='delete' data-cid='";
						rs+=list[i].cid+"'>删除</a>";
						rs+="</td>";
						rs+="<tr>";
						rs+="<td id='content' colspan='6'>"+list[i].ccontent+"</td>"
						rs+="</tr>";
						rs+="<tr><td colspan='6'><hr /></td></tr>";
					}
					$("#comments").empty();
					$("#comments").html(rs);
					var rs2="";
					rs2 += "当前页数:["+data.pageNow+"/"+data.pages+"]";
					$("#span1").html(rs2);
				}
			})
		
		
	   }
})
