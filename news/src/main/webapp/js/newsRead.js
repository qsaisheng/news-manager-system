/**
 * 
 */
$(function(){
		var start=0;
		var pageSize=3;
	    var nid;
		sideNews();
		checkLogin();
		readNews();
		var pagesBean;
		getComments();
		$("#logout").click(function(){
			$.ajax({
				url:"/news/getSessionServlet",
				data:{"action":"delete"},
			})
			window.location.href="/news/index.html";
			sessionStorage.removeItem("uname",data.uname);
	})
		$(".pageBar").click(function(){
			var action=$(this).data("type");
			if(action=="head"){
				start=0;
				getComments()
			}else if(action=="pre"){
				start=start < pageSize? 0:start-pageSize;
				getComments()
			}else if(action=="next"){
				start = (start+pageSize)>=pagesBean.counts?start:start+pageSize;
				getComments()
			}else if(action=="end"){
				start=pagesBean.counts%pageSize==0?start=pagesBean.counts-pageSize:pagesBean.counts-pagesBean.counts%pageSize;
				getComments()
			}
		});
	
		//获取左侧新闻
		function sideNews(){
			$.ajax({
				url:"/news/sideNewsSerlvet",
				success:function(data){
					var rs1="";
		            var rs2="";
		            var rs5="";
		            var list1=data.list1;
		            var list2=data.list2;
		            var list5=data.list5;
		            for (let i = 0; i < list1.length; i++) {
		                rs1+='<li>';
		                rs1+='<a href="/news/newsPages/newsRead.html?nid=';
		                rs1+=list1[i].nid;
		                rs1+='">';
		                rs1+=list1[i].ntitle;
		                rs1+='</a>';
		                rs1+='</li>';
		            }
		            for (let i = 0; i < list2.length; i++) {
		                rs2+='<li>';
		                rs2+='<a href="/news/newsPages/newsRead.html?nid=';
		                rs2+=list2[i].nid;
		                rs2+='">';
		                rs2+=list2[i].ntitle;
		                rs2+='</a>';
		                rs2+='</li>';
		            }
		            for (let i = 0; i < list5.length; i++) {
		                rs5+='<li>';
		                rs5+='<a href="/news/newsPages/newsRead.html?nid=';
		                rs5+=list5[i].nid;
		                rs5+='">';
		                rs5+=list5[i].ntitle;
		                rs5+='</a>';
		                rs5+='</li>';
		            }
		            $("#list1").empty();
		            $("#list2").empty();
		            $("#list5").empty();
		            $("#list1").html(rs1);
		            $("#list2").html(rs2);
		            $("#list5").html(rs5);
				}
			})
		}
		//检查是否登录
		function checkLogin(){
			$.ajax({
				url:"/news/getSessionServlet",
				data:{"action":"get"},
				dataType:"json",
				success:function(data){
					var username = data.uname;
					var rs="";
					if(username!=""&&username!=null){
						rs+="欢迎:"+username;
						rs+="<button id='logout' type='button' class='login_sub'>注销</button>";
						rs+="<a href='/news/index.html'>返回首页</a>";
						$("#top_login").html(rs);
						$("#logout").click(function(){
							$.ajax({
								url:"/news/userServlet",
								data:{"action":"logout"}
							})
							sessionStorage.removeItem("uname");
							window.location.href="/news/index.html";
						});
						$("#username").html(username);
						if(data.isbanned=="禁言"){
							$("#ccontent").html("你已被禁言");
							$("#submit").css("display","none");
						}
					}
				}
			})
			}
			
			

		//获取当前页面的新闻详情
		function readNews(){
			nid=getQueryVariable("nid");
			$.ajax({
				url:"/news/newsServlet",
				data:{"nid":nid,"action":"selectOne"},
				success:function(data){
					$("#newsTitle").html(data.ntitle);
					$("#newsAuthor").html(data.nauthor);
					$("#newsCreateDate").html(data.nmodifyDate);
					$("#newsContent").html(data.ncontent);
					var path="/news/upload/"+data.npicPath;
					document.getElementById("image").src=path;
				}
			})
		}
		//获取url的参数值
	   function getQueryVariable(variable){
	       var query = window.location.search.substring(1);
	       var vars = query.split("&");
	       for (var i=0;i<vars.length;i++) {
	               var pair = vars[i].split("=");
	               if(pair[0] == variable){return pair[1];}
	       }
	       return(false);
	   };
	   function getComments(){
			$.ajax({
				url:"/news/commentsSerlvet",
				dataType:"json",
				data:{"action":"selectAll",	"start":start,"pageSize":pageSize,"cnid":getQueryVariable("nid")},
				success:function(data){
					pagesBean=data;
					var list=data.lists;
					var rs="";
					var rs2="";
					if(list.length>0){
						for(var i=0;i<list.length;i++){
							rs+="<tr>";
							rs+="<td>留言人:</td>";
							rs+="<td id='name'>"+list[i].cauthor+"</td>";
							rs+="<td>ip:"+list[i].cip+"</td>";
							rs+="<td>留言时间:</td>";
							rs+="<td id='time'>"+list[i].cdate+"</td>"
							rs+="<tr>";
							rs+="<td id='content' colspan='6'>"+list[i].ccontent+"</td>"
							rs+="</tr>";
						}
						$("#comments").empty();
						$("#comments").html(rs);
						rs2 += "当前页数:["+data.pageNow+"/"+data.pages+"]";
						$("#span1").html(rs2);
					}else{
						$("#comments").empty();
						$("#span1").html("当前新闻没有评论").css("color","blue");
					}

				}
			})
			

}
	$("#submit").click(function(){
			var ccontent=$("#ccontent").val();
			var username=$("#username").text();
			if(ccontent!=""){
				$.ajax({
					url:"/news/commentsSerlvet",
					data:{"action":"addComments","cnid":getQueryVariable("nid"),"ccontent":ccontent,"username":username},
					success:function(data){
						
						if(data=="true"){
							start=0;
							getComments();
						}else{
							alert("添加失败");
					}
				}
			})
			}else{
				alert("不能上传空内容");
			}
	   });
		//设置login事件
		$("#login").click(function (){
			var uname=$("#uname").val();
			var upwd=$("#upwd").val();
			if(uname!=""&&upwd!=""){
				$.ajax({
					url:"/news/userServlet",
					dataType:"json",
					data:{"action":"login","uname":uname,"upwd":upwd},
					success:function(data){
						sessionStorage.setItem("uname",data.uname);
						checkLogin();
						var rs= "";
						if(data.type==1){
							rs+="欢迎:"+data.uname;
							rs+="<button id='logout' type='button' class='login_sub'>注销</button>";
							$("#top_login").empty();
							$("#top_login").html(rs);
							$("#logout").click(function(){
								$.ajax({
									url:"/news/getSessionServlet",
									data:{"action":"delete"},
									success:function(){
										window.location.href="/news/index.html";
									}
								})
								
							});
							
							}else if(data.type==-1){
									window.location.href="/news/adminPage/admin.html";
									sessionStorage.setItem("uname",data.uname);
							}else{
								$("#error").html("用户名或密码错误").css("color","red");
						}
					}			
				})
			}else{
				$("#error").html("用户名或密码不能为空").css("color","red");
			}
		});


})
