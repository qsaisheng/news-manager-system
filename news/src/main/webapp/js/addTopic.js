/**
 * 
 */
$(function(){
	checkLogin();
	check();
	function check(){
		var tname=$("#tname").val();
		if(tname==""){
			$("#addTopicSubmit").css("display","none");
		}
	}
	function checkLogin(){
			var username = sessionStorage.getItem("uname");
			rs="";
			$.ajax({
				url:"/news/getSessionServlet",
				data:{"action":"get"},
				dataType:"json",
				success:function(data){
					if(data.uname!=""){
						rs+="欢迎:"+username;
						rs+="<button id='logout' type='button' class='login_sub'>注销</button>";
						if(username=="admin"){
							rs+="<a href='/news/adminPage/admin.html'>进入后台</a>"
						}
						$("#top_login").html(rs);
						$("#logout").click(function(){
							$.ajax({
								url:"/news/getSessionServlet",
								data:{"action":"delete"},
								success:function(){
									window.location.href="/news/index.html";
								}
							})
						});
					}
				}
				
			})
	}
	$("#tname").keyup(function(){
		check();
		var flag=0;
		var tname=$("#tname").val();
		if(tname!=""){
			$.ajax({
				url:"/news/topicsServlet",
				data:{"action":"selectOneByName","tname":tname},
				success:function(data){
					if(data=="false"){
						$("#error").html("主题已存在").css("color","red");
						$("#addTopicSubmit").css("display","none");
					}else{
						$("#error").html("主题可添加").css("color","green");
						$("#addTopicSubmit").css("display","inline");
					}
				}
			})
		}else{
			$("#addTopicSubmit").css("display","none");
		}

	})
})